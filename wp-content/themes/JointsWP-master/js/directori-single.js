function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}


jQuery(document).ready(function() {
	updateDotdotdot();
	$("section.sectors > p").dotdotdot({
		height : 65,
		fallbackToLetter : true,
		watch : true
	});

});
