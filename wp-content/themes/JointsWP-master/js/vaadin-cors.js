XMLHttpRequest.prototype._originalSend = XMLHttpRequest.prototype.send;
var sendWithCredentials = function(data) {
	this.withCredentials = true;
	this._originalSend(data);
};
XMLHttpRequest.prototype.send = sendWithCredentials;