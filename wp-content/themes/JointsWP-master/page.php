<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">
			
		<div class="large-12 small-12 columns">
					<div class="box-head">
						<span class=link-to-map><a href="#" class="button">VES AL MAPA <span class="icon-ic_room_48px"></span></a></span>
					</div>
		</div>
		    <main id="main" class="large-8 medium-8 columns" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page-normal' ); ?>

			    <?php endwhile; endif; ?>

			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
