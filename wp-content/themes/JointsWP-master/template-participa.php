<?php /* Template Name: PARTICIPA */ ?>

<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<div class="box-head">
	      <h1 class="page-title">Participa</h1>
          <?php echo addVesAlMapa(); ?>
	    </div>


		    <main id="main" class="large-12 small-12 columns participa" role="main">

			<!-- bloc 01 -->
				<div class="row out">
					<div class="celito left purple"></div>
					<div class="celito right purple"></div>

						<div class="large-6 small-12 columns">
							<a href="<?php echo get_site_url(); ?>/xinxeta/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/foto1.png" alt="Pamapam" /></a>
							<h3><a href="<?php echo get_site_url(); ?>/xinxeta/">Vols ser xinxeta?</a></h3>
						</div>
						<div class="large-6 small-12 columns">
							<p><a href="<?php echo get_site_url(); ?>/xinxeta/">Et formaràs en economia solidària, aniràs a entrevistar iniciatives i les pujaràs al mapa per a donar-les a conèixer!</a></p>
							<a href="<?php echo get_site_url(); ?>/xinxeta/"><span class="icon-arrow-right"></span></a>
						</div>
				</div> <!-- END bloc 01 -->

				<!-- bloc 02 -->
					<div class="row out">
						<div class="celito left green"></div>
						<div class="celito right green"></div>
							<div class="large-6 small-12 columns">
								<?php $upload_dir=wp_upload_dir();
                                      $upload_dir=$upload_dir['baseurl'];
                                ?>
                                <a href="<?php echo get_site_url(); ?>/comunitat-digital/"><img src="<?php echo $upload_dir; ?>/2019/02/comunitat_digital.png" alt="Pamapam" /></a>
								<h3><a href="<?php echo get_site_url(); ?>/comunitat-digital/">Vols formar part de la comunitat digital?</a></h3>
							</div>
							<div class="large-6 small-12 columns">
								<p><a href="<?php echo get_site_url(); ?>/comunitat-digital/">La plataforma digital de Pam a  Pam és oberta i col·laborativa. Participa a la nostra comunitat digital per millorar-la i aprendre plegades!</a></p>
								<a href="<?php echo get_site_url(); ?>/comunitat-digital/"><span class="icon-arrow-right"></span></a>
							</div>
					</div> <!-- END bloc 02 -->

					<!-- bloc 03 -->
					<div class="row out">
						<div class="celito left orange"></div>
						<div class="celito right purple"></div>
							<div class="large-6 small-12 columns">
								<a href="<?php echo get_site_url(); ?>/afegeix-iniciativa/"><img src="<?php echo $upload_dir; ?>/2019/02/afegeix_punt.png" alt="Pamapam" /></a>
								<h3><a href="<?php echo get_site_url(); ?>/afegeix-una-iniciativa/">Vols proposar un nou punt?</a></h3>
							</div>
							<div class="large-6 small-12 columns">
								<p><a href="<?php echo get_site_url(); ?>/afegeix-iniciativa/">Coneixes alternatives de consum responsable al teu barri? Ets una iniciativa d’economia solidària i t’agradaria aparèixer al web? Pots proposar noves iniciatives que anirem a entrevistar!</a></p>
								<a href="<?php echo get_site_url(); ?>/afegeix-iniciativa/"><span class="icon-arrow-right"></span></a>
							</div>
					</div> <!-- END bloc 03 -->



			</main> <!-- end #main -->


			<!-- Efectes caixes, TODO: incorporar-ho al js que es consideri -->
			    <script>
			    $('.row.out a img, .row.out h3 a, .row.out p a, .row.out span').hover(function() {
			  		var outWidget = $(this).parents('.row.out');
			  		$(outWidget).addClass("visualEffect");
			  	});
			  	$('.row.out a img, .row.out h3 a, .row.out p a, .row.out span').mouseout(function() {
			  		var outWidget2 = $(this).parents('.row.out');
			  		$(outWidget2).removeClass("visualEffect");
			  	});
			    </script>


		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

	<div class="contain-to-grid sand-bkg">
	  <?php get_template_part( 'parts/include', 'afiliation' ); ?>
	</div>

<?php get_footer(); ?>
