<div class="large-4 medium-6 columns">
	<div class="ima-pagina-ficha"><img src="<?php echo $baseApiUrl . $entity->pictureUrl; ?>" /></div>
    <div class="overlay" onClick="style.pointerEvents='none'"></div>
    <div id="map" style="height: 400px;"></div>
	<script>
		var nombre = "<?php echo addslashes($entity->name); ?>";
		var marker1 = "<?php echo $entity->latitude; ?>";
		var marker2 = "<?php echo $entity->longitude; ?>";

		var osmUrl = "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png";
		var osmAttrib = "Map data © <a href=\"http://maps.wikimedia.org\">Wikimedia Map</a> contributors";
		var osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
		var map = L.map('map').setView([marker1, marker2], 17).addLayer(osm);
		L.marker([marker1, marker2])
			.addTo(map)
			.bindPopup(nombre)
			.openPopup();
    </script>
</div>
<main id="main" class="large-4 medium-4 columns single-xinxeta" role="main">
<article role="article">
	<header class="article-header">
		<h1 class="entry-title single-title"><?php echo $entity->name; ?></h1>
	</header>
	<ul class="llista-sectors">
			<li><img class="disabled" src="<?php echo $baseApiUrl . $entity->mainSector->iconUrl; ?>"<?php echo $entity->mainSector->name; ?>>
			<?php echo $entity->mainSector->name; ?></li>

<?php
		foreach($entity->sectors as $sector) {
?>
			<li><img class="disabled" src="<?php echo $baseApiUrl . $sector->iconUrl; ?>"<?php echo $sector->name; ?>>
			<?php echo $sector->name; ?></li>

<?php
		}
?>
    </ul>
	<section class="entry-content" itemprop="articleBody">
		<p><?php echo $entity->description; ?></p>
	</section>
	<hr>
	<section class="adress">
		<div class="vcard">
			<p class="adr">
				<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;<span class="street-address"><?php echo $entity->address; ?></span>
				<span class="area"><?php echo $entity->neighborhood->name; ?></span><br>
				<span class="locality"><?php echo $entity->town->name; ?></span><br>
				<span class="region">(<?php echo $entity->province->name; ?>)</span><br>
			</p>


			<p class="schedule"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $entity->openingHours; ?></p>
			<p>
			<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;<span class="tel"><?php echo $entity->phone; ?></span><br/>
			<i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;<span class="email"><?php echo $entity->email; ?></span><br/>
			<i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;<span class="web"><a href="<?php echo $entity->web; ?>" target="_blank"><?php echo $entity->web; ?></a></span>
		</p>
		</div>
	</section>
	<hr>
	<div class="social">
		<strong>Xarxes</strong><br/>
<?php
		if (!empty($entity->facebook)) {
?>
			<span class="fn"><a href="<?php echo $entity->facebook; ?>"target="_blank"><span class="icon-facebook"></span></a></span>
<?php
		}
		if (!empty($entity->twitter)) {
?>
			<span class="twitter"><a href="<?php echo $entity->twitter; ?>"target="_blank"><span class="icon-twitter"></span></a></span>
<?php
		}
		if (!empty($entity->googlePlus)) {
?>
			<span class="googleplus"><a href="<?php echo $entity->googlePlus; ?>"target="_blank"><span class="icon-google-plus"></span></a></span>
<?php
		}
		if (!empty($entity->instagram)) {
?>
			<span class="instagram"><a href="<?php echo $entity->instagram; ?>"target="_blank"><span class="icon-instagram"></span></a></span>
<?php
		}
		if (!empty($entity->pinterest)) {
?>
			<span class="pinterest"><a href="<?php echo $entity->pinterest; ?>"target="_blank"><span class="icon-pinterest"></span></a></span>
<?php
		}
		if (!empty($entity->quitter)) {
?>
			<span class="quitter"><a href="<?php echo $entity->quitter; ?>"target="_blank"><span class="icon-quitter"></span></a></span>
<?php
		}
?>
	</div>
</article>
</main>

<div class="large-4 small-12 sidebar-criteris columns">
	<p class="titol-sidebar">Criteris</p>
	<ul class="criteris">
<?php
		$criterionLevels = array();
		if ($entity->entityEvaluation) {
			foreach ($entity->entityEvaluation->entityEvaluationCriterions as $entityEvaluationCriterion) {
				$criterionLevels[$entityEvaluationCriterion->criterion->id] = $entityEvaluationCriterion->level;
			}
		}

		$imageWidth = "width=\"24\"";
		if ($entity->oldVersion) {
			$imageWidth = "";
		}

		foreach ($criteria as $criterion) {
			$criterionLevel = $criterionLevels[$criterion->id];
			if ($criterionLevel === NULL || $criterionLevel < 0) {
?>
				<li class='cero'><img class="disabled" src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/><p><?php echo $criterion->name; ?></p></li>
<?php
			} else if ($criterionLevel == 0) {
?>
				<li><img class="disabled" src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/><p><?php echo $criterion->name; ?></p></li>
<?php
			} else {
?>
				<li>
					<img src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/><p><?php echo $criterion->name; ?></p>
					<div class="dots">
<?php
				$dotsCount = round($criterionLevel / 100, 0, PHP_ROUND_HALF_DOWN);
				for ($i = 0; $i < $dotsCount; $i++) {
?>
						<span class="do"></span>
<?php
				}
?>
					</div>
<?php
			}
		}
?>
	</ul>
	<p class="orange tit-bold">Pam a Pam és el mapa col·laboratiu de l'Economia Solidària a Catalunya. Una eina per a la transformació social, on mostrem iniciatives que compleixen els criteris d'ESS per facilitar-te el consum responsable.</p>
</div>
